package com.gmail.ivansergeish.wordchain.way;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gmail.ivansergeish.wordchain.strategy.Strategy;
import com.gmail.ivansergeish.wordchain.strategy.StrategyForTheSameLength;

public class SimpleWay implements Way {
	private static final int MAX_WAY_LENGTH = 100;
	private List<List<String>> chains = new ArrayList();
	@Override
	public List<String> findWay(String strFirst, String strLast,
			List<String> strs) {
		Strategy strategy = new StrategyForTheSameLength();
		List<String> firstChain = new ArrayList();
		firstChain.add(strFirst);
		chains.add(firstChain);
		for (int i = 0; i < MAX_WAY_LENGTH; i++) {		
			List<List<String>> newChains = new ArrayList();
			for (List<String> chain : this.chains) {
				List<String> nextSteps = findNeigbors(strs, strategy, chain.get(chain.size()-1));
				if (nextSteps.contains(strLast)) {
					chain.add(strLast);
					return chain;
				}
				newChains.addAll(addNextStepToChain(chain,nextSteps));
			}
			chains = new ArrayList(newChains);
		}
		return null;
	}
	
	private List<String> findNeigbors(List<String> strs, Strategy strategy, String value) {
		if (strs == null) {return new ArrayList();}
		List<String> nodes = new ArrayList();
		Iterator iterator = strs.iterator();
		while(iterator.hasNext()) {
			String str = (String) iterator.next();
			if (strategy.check(str, value)) {
				nodes.add(str);
			}
		}
		nodes.remove(value);
		return nodes;
	}
	private List<List<String>> addNextStepToChain(List<String> chain, List<String> nextSteps) {
		List<List<String>> result = new ArrayList();
			for (String next : nextSteps) {
				List<String> newChain = new ArrayList<String>(chain);
				newChain.add(next);
				result.add(newChain);
			}
		return result;		
	}
}
