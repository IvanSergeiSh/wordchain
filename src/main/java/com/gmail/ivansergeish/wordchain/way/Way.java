package com.gmail.ivansergeish.wordchain.way;

import java.util.List;

public interface Way {
	public List<String> findWay(String strFirst, String strLast, List<String> strs);
}
