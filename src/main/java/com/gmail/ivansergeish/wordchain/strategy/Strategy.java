package com.gmail.ivansergeish.wordchain.strategy;

public interface Strategy {
	public boolean check(String str1, String str2);
}
