package com.gmail.ivansergeish.wordchain.strategy;

public class StrategyForTheSameLength implements Strategy{

	@Override
	public boolean check(String str1, String str2) {
		if(str1 == null || str2 == null) { return false;}
		if(str1.length() != str2.length()) {return false;}
		if(str1.length() == 0 || str2.length() == 0) {return false;}
		int counter = 0;
		for(int i =0; i< str1.length(); i++) {
			if(str1.charAt(i) != str2.charAt(i)) {counter = counter + 1;}
			if (counter > 1) {return false;}
		}
		return true;
	}
	
}
