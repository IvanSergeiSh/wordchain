package com.gmail.ivansergeish.application;

import java.io.IOException;
import java.util.List;

import com.gmail.ivansergeish.dictionary.Dictionary;
import com.gmail.ivansergeish.wordchain.way.SimpleWay;
import com.gmail.ivansergeish.wordchain.way.Way;

public class Application {
	private static final String F_NAME = "src/main/resources/dictiopnary.txt";
	public static void main(String[] args) {
		if (args == null || args.length == 0) {
			System.out.println("arguments should contain first word, last word");
		}
		try {
			Dictionary dictionary = new Dictionary(F_NAME);
			List<String> strs = dictionary.getDictionary();
			Way way = new SimpleWay();
			List<String> chain = way.findWay(args[0], args[1], strs);
			if (chain == null || chain.size() == 0) {
				System.out.println("No chain found");
			} else {
				System.out.println(chain);
			}
			
		} catch (IOException e) {
			System.out.println("No such file");
			e.printStackTrace();
		}
		

	}

}
