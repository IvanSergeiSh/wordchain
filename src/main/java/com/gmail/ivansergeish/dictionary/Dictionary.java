package com.gmail.ivansergeish.dictionary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Dictionary {
	private List<String> dictionary;

	public Dictionary(String fName) throws IOException {
		dictionary = new ArrayList();
		dictionary = Files.readAllLines(Paths.get(fName));
	}
	public List<String> getDictionary() {
		return dictionary;
	}
}
