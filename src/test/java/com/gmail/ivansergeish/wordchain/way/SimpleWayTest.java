package com.gmail.ivansergeish.wordchain.way;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gmail.ivansergeish.wordchain.strategy.Strategy;
import com.gmail.ivansergeish.wordchain.strategy.StrategyForTheSameLength;

public class SimpleWayTest {
	private static final String FIRST_WORD = "123";
	private static final String LAST_WORD = "114"; 
	private List<String> strs = new ArrayList(Arrays.asList("123","124","114", "545"));
	private List<String> strs1 = new ArrayList(Arrays.asList("123","223", "224", "114", "124", "545"));
	private SimpleWay way; 
	@Before
	public void init() {
		way = new SimpleWay();
	}
	@Test
	public void testFindWay() {
		List<String> strChain = way.findWay(FIRST_WORD, LAST_WORD, strs);
		Assert.assertTrue(strChain.size() == 3);
		Assert.assertTrue(strChain.get(0) == "123");
		Assert.assertTrue(strChain.get(1) == "124");
		Assert.assertTrue(strChain.get(2) == "114");
	}
	
	@Test
	public void testFindWay_TWO_WAYS() {
		List<String> strChain = way.findWay(FIRST_WORD, LAST_WORD, strs1);
		Assert.assertTrue(strChain.size() == 3);
		Assert.assertTrue(strChain.get(0) == "123");
		Assert.assertTrue(strChain.get(1) == "124");
		Assert.assertTrue(strChain.get(2) == "114");
	}
	@Test
	public void testAddNextStepToChain() {
		List<String> chain = new ArrayList();
		chain.add("first");
		List<String> strs = new ArrayList();
		strs.add("second1");
		strs.add("second2");
		try {
			Method method = way.getClass().getDeclaredMethod("addNextStepToChain", List.class,List.class);
			method.setAccessible(true);
			List<List<String>> result = (List<List<String>>) method.invoke(way, chain, strs);
			Assert.assertTrue(result.size() == 2);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFindNeigbors() {
		Method method;
		try {
			method = way.getClass().getDeclaredMethod("findNeigbors", List.class, Strategy.class, String.class);
			method.setAccessible(true);
			List<String> nodes = (List<String>) method.invoke(way, strs1, new StrategyForTheSameLength(),"123");
			Assert.assertNotNull(nodes);
			Assert.assertEquals(2, nodes.size());
			Assert.assertTrue(nodes.contains("124"));
			Assert.assertTrue(nodes.contains("223"));
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test() throws IOException{
		List<String> dictionary = new ArrayList();
		dictionary = Files.readAllLines(Paths.get("src/test/resources/test.txt"));
		System.out.println(dictionary);
		
	}
}
