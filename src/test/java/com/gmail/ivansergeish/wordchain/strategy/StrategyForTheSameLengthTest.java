package com.gmail.ivansergeish.wordchain.strategy;



import org.junit.Assert;
import org.junit.Test;

public class StrategyForTheSameLengthTest extends StrategyForTheSameLength{
	@Test
	public void testCheck() {
		Assert.assertFalse(super.check("cat", "dog"));
		Assert.assertTrue(super.check("cat", "cab"));
		Assert.assertFalse(super.check("cast", "cat"));
		Assert.assertTrue(super.check("123", "223"));
		Assert.assertTrue(super.check("123", "123"));
		
		Assert.assertFalse(super.check("", ""));
		Assert.assertFalse(super.check("1", ""));
		Assert.assertFalse(super.check("", "cat"));
		Assert.assertFalse(super.check(null, "cat"));
		Assert.assertFalse(super.check(null, null));
		Assert.assertFalse(super.check("cast", null));
		
	}
}
