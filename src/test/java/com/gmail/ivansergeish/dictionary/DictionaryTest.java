package com.gmail.ivansergeish.dictionary;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class DictionaryTest {
	private static final String F_NAME = "src/test/resources/test.txt";
	
	@Test
	public void testGgetDictionary() throws IOException {
		Dictionary dictionary = new Dictionary(F_NAME);
		List<String> strs = dictionary.getDictionary();
		Assert.assertTrue(strs.size() == 5);
		Assert.assertTrue(strs.get(0).equals("123"));
		Assert.assertTrue(strs.get(4).equals("114"));
	}
}
